 function [du , dh, ds, dg, dcv] = dep_GCEOS( u_EOS, w_EOS    ...
                                           , aalpha, daalpha, ddaalpha  ...
                                           , b, temp, vol )
% DEP_GCEOS  Calculate departure functions for generalized cubic EOS
%
% Inputs: 
%    u_EOS, w_EOS  - Parameter for generalized cubic EOS,  see e.g. REF1 & REF2 
%    aalpha,
%    daalpha,
%    ddaalpha  - a*alpha(T) in cubic EOS
%    b         - Co-volume in cubic EOS
%    temp      - Temprature
%    vol       - Volume
%
% Outputs:
%    du      - internal energy departure
%    dh      - enthalpy departure
%    ds      - entropy departure
%    dg      - Gibbs energy departure
%    dcv     - Heat capacity at const. volume departure
%
% Authors: 
%    Jan Matheis, Technical University Munich, jan.matheis@gmail.com
% 
% Literature:
%    REF1: Poling et al. The Properties of Gases and Liquids
%    REF2: Kim et al. (2012) Thermodynamic modeling based on a generalized cubic equation of state for kerosene/LOx rocket combustion. doi: 10.1016/j.combustflame.2011.10.008 

global R_univ
                                                                                                            
uEOSwEOS = sqrt( u_EOS^2 - 4. * w_EOS );

right =  log( ( 2. * vol + b * ( u_EOS  - uEOSwEOS ) ) ...
            / ( 2. * vol + b * ( u_EOS  + uEOSwEOS ) ) );

% Pressure
p = R_univ * temp / (vol - b) ...
  - aalpha / ( vol^2 + u_EOS * b * vol + w_EOS * b^2 ); 

K = 1. / (b * uEOSwEOS) * right;

% Internal energy departure
du = (aalpha - temp * daalpha) * K; 

% Entropy departure
ds = - daalpha * K + R_univ * log( ( 1 - b / vol ) );

% Enthalpy departure
dh = du + p * vol - R_univ * temp; 

% Gibbs Energy departure
dg = dh - temp * ds;

% Cv departure
dcv = - temp * ddaalpha * K;

end 