function [dgmix, phase_m] = evaldgmix( nc, z, temp, press )
% EVALDGMIX   Calculate Gibbs energy of mixing
%
% Inputs: 
%    nc           - Number of components
%    z_test(nc)   - Overall composition z
%    temp, press  - Temperature and pressure
%
% Outputs:
%    dgmix        - Gibbs energy of mixing
%    phase_m      - Guess what this phase might be, see vol_GCEOS
%
% Author: 
%    Jan Matheis, Technical University Munich, jan.matheis@gmail.com
%
% Literature:
%    REF1: Firoozabadi (1999) Thermodynamics of Hydrocarbon Reservoirs (S.220 & S.287)
%    REF2: Michelsen & Mollerup (2007) Thermodynamic Models: Fundamentals & Computational Aspects (S.224)
%    REF3: Elliott & Lira (2012) Introductory Chemical Engineering Thermodynamics (Eq. 10.47-10.50 / 11.22)

% 0 == pure component and NOT ideal reference state
% ln(f_i) = ln(phi_i) + ln(y*p)
% (nu_i - nu_i^0)/RT = ln(f_i/f_i^0) = ln(phi_i)+ln(y_i*p) - (ln(phi_i^0)+ln(1*p))

global R_univ
global u_EOS w_EOS
global slst

dgmix = 0.;

% Calculate mixture params for composition z and evaluate the EOS:
[aalpha_m, ~, ~, b_m, aalpha_jk_m] = mix_GCEOS( nc, z, temp );
[vol_m, phase_m] = vol_GCEOS( u_EOS, w_EOS, aalpha_m, b_m, temp, press, 'Gibbs' );
Z_m = vol_m(1) * press / ( R_univ * temp );


for i = 1:nc

    % Calculate ln[ phi^0(T,p,z_i=1) ] (Pure component value):
    work = zeros(nc,1); work(i) = 1; 
    [aalpha_i, ~, ~, b_i, ~] = mix_GCEOS( nc, work, temp );

    [vol_0, ~] = vol_GCEOS( u_EOS, w_EOS, aalpha_i, b_i, temp, press, 'Gibbs' );
    Z_0 = vol_0(1) * press / ( R_univ * temp );
    lnphi0_i = lnphi_GCEOS( u_EOS, w_EOS, b_i, b_i, aalpha_i, aalpha_i, Z_0, temp, press );
    
    % Calculate ln[ phi(T,p,z_i) ]:
    lnphi_i  = lnphi_GCEOS( u_EOS, w_EOS, slst(i).b, b_m, aalpha_jk_m(i), aalpha_m, Z_m, temp, press );
    
    % Gibbs energy of mixing:
    dgmix = dgmix + z(i) * ( lnphi_i + log( z(i) ) - lnphi0_i );
    
end

