function c_i = c_PR( omega_i, ~ )
% C_PR  Calculate "c" parameter in alpha function of Peng-Robinson EOS
%
% Inputs: 
%    omega     -  Acentric factor
%
% Outputs:
%    c_i       - "c" parameter alpha function of Peng-Robinson EOS
%
% Author: Jan Matheis, Technical University Munich, jan.matheis@gmail.com

c_i = 0.37464 ...
    + 1.54226 *   omega_i ...
    - 0.26992 * ( omega_i )^2;

end

