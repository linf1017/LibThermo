function [h, cp] = NASA( T, ap )
% NASA  Calculate ideal reference state from NASA polynomials.
%       Only for enthalpy and specific heat at constant pressure. 
%
% Inputs: 
%    T         - Temperature
%    ap        - Coefficients
%
% Outputs:
%    h      - enthalpy
%    cp      - specific heat at constant p 
%
% Authors: 
%    Jan Matheis, Technical University Munich, jan.matheis@gmail.com
% 
% Literature:
%    REF1: E. Goos, A. Burcat, and B. Ruscic. Third Millennium Ideal Gas 
%          and Condensed Phase Thermochemical Database for Combustion, 2009. 
%          URL http://burcat. technion.ac.il/dir/.

logT  = log(T);
T_inv = 1./T;

cp = T_inv * ( ap(2) + T_inv * ap(1) ) ...
           +           ap(3) ...
           + T     * ( ap(4) ...
           + T     * ( ap(5) ...
           + T     * ( ap(6) + T * ap(7) ) ) ) ;

h   = ( ap(8) - ap(1) * T_inv + ap(2) * logT ...
    + T * ( ap(3) ...
    + T * ( 0.5 * ap(4) ... 
    + T * (       ap(5) / 3. ...
    + T * ( 0.25 * ap(6) ...
    + T * 0.2 * ap(7) ) ) ) ) ) ;

end
