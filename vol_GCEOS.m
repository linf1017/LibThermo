function [vol, phase] = vol_GCEOS( u_EOS, w_EOS, aalpha_EOS, b_EOS, temp, press, root )
% GETVOLUME  Calculate volume/compressibility factor from cubic EOS  
%            Z^3 + C2 * Z^2 + C1 * Z + C0 = 0
%
% Inputs: 
%    u_EOS, w_EOS    - Parameter for generalized cubic EOS,  see e.g. REF1 & REF2 
%    aalpha_EOS      - a*alpha(T) in cubic EOS
%    b_EOS           - Co-volume in cubic EOS
%    temp            - Temprature
%    press           - Pressure
%    root (optional) - Which root? 
%                      Options: 1) 'Gibbs': If two volume roots are found 
%                                           return the one with the lower 
%                                           Gibbs energy 
%                               2) 'Liq': If two volume roots are found
%                                         return smaller volume root
%                               3) 'Vap': If two volume roots are found
%                                         Return larger volume root
%                                         
% Outputs:
%    vol             - Molar volume
%    phase           - "Guess" if vol corresponds to liquid or vapor state 
%
% Author: Jan Matheis, Technical University Munich, jan.matheis@gmail.com
%
% Please cite this work as: 
%    J. Matheis and S. Hickel (2017), Multi-component vapor-liquid equilibrium 
%    model for LES of high-pressure fuel injection and application to ECN Spray A
%    Int. J. Multiph. Flow, doi: 10.1016/j.ijmultiphaseflow.2017.11.001
%
% Literature:
%    REF1: Numerical recipes in fortran 77: The art of scientific computing
%    REF2: Introductory Chemical Engineering Thermodynamics, J. Richard Elliott, Carl T. Lira
%    REF3: Michelsen & Mollerup (2007) Thermodynamic Models: Fundamentals & Computational Aspects

global R_univ

if nargin < 7
    root = 'Gibbs'; % Default root mode
end

eindrittel = 1./3.;
uEOSwEOS   = sqrt( u_EOS^2 - 4. * w_EOS ); %! for PR and SRK this is parameter that will never change

% Short cuts
A = ( aalpha_EOS * press ) / ( R_univ * temp )^2;
B = ( b_EOS * press ) / ( R_univ * temp );
%
C2  = ( B * ( u_EOS - 1. ) - 1. );
C1  = ( A + B * ( w_EOS * B - u_EOS * B - u_EOS ) );
C0  = - B * ( w_EOS * B^2 + w_EOS * B + A );


Q = ( C2^2 - 3.*C1 ) / 9.;
R = ( 2.*C2^3 - 9.*C2*C1 +27.*C0 ) / 54.;

% Three real roots may exist
if R^2 < Q^3

   sqrtQ = sqrt(Q);
   phi = acos( R / ( sqrtQ * Q ) );
   x1 = -2. * sqrtQ * cos(eindrittel*phi)        - eindrittel*C2;
   x2 = -2. * sqrtQ * cos(eindrittel*(phi+2*pi)) - eindrittel*C2;
   x3 = -2. * sqrtQ * cos(eindrittel*(phi-2*pi)) - eindrittel*C2;

   Z = [x1, x2, x3];

   if min( Z ) < B % Volume cannot be smaller than the co-volume
       % There is only one physical meaningful volume root (out of three real roots)
       Z     = [max( Z   ), -1.];

       % Somehow random choice as seen in a example program for solving cubic EOS
       % of Michelsen and Mollerup (http://www.tie-tech.com/demo/Michelsen_Mollerup_CD.zip) 
       if Z(1) < 3.5 * B 
            phase = 'Liq';
       else
            phase = 'Vap';
       end
       
   else

       Z   = sort(Z);  % Sort from liquid to vapor (small to large)
       Z   = [Z(1), Z(3)]; % Center root is thermodynamically meaningless

       % Because there are two real roots: 
       % > Find the most stable one by looking at Gibbs free energy.
       % > The volume with the lower Gibbs energy is stable
       if strcmp(root,'Gibbs')   
           
           work2 = B * ( u_EOS + uEOSwEOS );
           work1 = B * ( u_EOS - uEOSwEOS );
           Z_l = Z(1);
           Z_v = Z(2);
           dg = log( ( Z_l - B ) / ( Z_v - B ) ) ...
              + A / ( B * uEOSwEOS )  ...               
              * log( ( 2 * Z_l + work2 )*( 2 * Z_v + work1 ) ...
                   / ( 2 * Z_l + work1 )/( 2 * Z_v + work2 ) ) ...     
              - ( Z_l - Z_v );

          if dg < 0  % g_l > g_v
               Z = fliplr(Z);
               phase = 'Vap';
           else % g_v > g_l
               phase = 'Liq';
          end    
          
       % Select the liquid root
       elseif strcmp(root,'Liq')
           
           phase = 'Liq';
           
       % Select the vapor root 
       elseif strcmp(root,'Vap')
           
           Z   = fliplr(Z);
           phase = 'Vap'; 
           
       end   
   end

% One real root, two imaginary roots exist
else 
    E = - R / abs(R) * ( abs(R) + sqrt( R^2 -Q^3 ) )^eindrittel;

    if E == 0. 
        F=0.;
    else
        F=Q/E;
    end
    
    Z = ( E + F - eindrittel * C2 );
        
    if Z < 3.5 * B
        phase = 'Liq';
    else
        phase = 'Vap';
    end
    
    Z     = [Z, -1];

end % Real/Imag roots

vol = Z * R_univ * temp / press; % Get volume 

end
