% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% Author: Jan Matheis, Technical University Munich, jan.matheis@gmail.com
% Info: Calculate frozen and equilibrium mixing temperature for the binary 
%       sytem Nitrogen (N2) and Hydrogen (H2). The main buidling blocks are 
%       (i) cubic equations of state (ii) thermodynanmic stability 
%       analysis via TPD analysis and (iii) vapor-liquid equilibrium 
%       calculations. 
% Please cite this work as: 
% J. Matheis and S. Hickel (2017), Multi-component vapor-liquid equilibrium 
% model for LES of high-pressure fuel injection and application to ECN Spray A
% Int. J. Multiph. Flow, doi: 10.1016/j.ijmultiphaseflow.2017.11.001
% Please see for changes under https://gitlab.com/jmatheis/LibThermo
% Tested with MATLAB R2015a 64-bit (maci64).
% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% Nomenclature:
% Y =: overall mass fraction
% z =: overall mole fraction
% x =: liquid phase composition on a molar basis
% y =: vapor phase composition on a molar basis
% w =: trial phase composition on a molar basis of TPD test (or w_trial) 
% ... 
% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

clear all
close all
clc

global R_univ EOS u_EOS w_EOS delta_1 delta_2
global nc
global slst k_ij
global crule 
global alpha_function
global K_init
 
R_univ  = 8314.472;  % J kmol^-1 K^-1
nc      = 2;    % Number of components
EOS     = 'PR'; % Which EOS do we solve?
                % Options: PR  =: Peng-Robinson

plot_TPD = true;                
                
if strcmp(EOS,'PR') % Peng-Robinson EOS
    delta_1 =  1 + sqrt(2.); 
    delta_2 =  1 - sqrt(2.);  
    u_EOS   =  2; % = delta_1 + delta_2 
    w_EOS   = -1; % = delta_1 * delta_2
else 
    error('Please implement %s on your own.',EOS)
end

crule = 'CRITICAL'; % Which combination rule do we use?
                    % CRITICAL := Pseudo critical method 
                    
alpha_function = 'DEF'; % Which alpha function do we use?
                        % DEF := default alpha function for PR EOS
% Component #1
slst(1).Name         = 'H2';
slst(1).M            = 2.016;           % kg kmol^-1
slst(1).omega        = -0.217;          % -
slst(1).pc           = 1.293e6;         % Pa 
slst(1).Tc           = 32.98;           % K 
slst(1).rhoc         = 15.508;          % kmol m^-3
slst(1).vc           = 1./slst(1).rhoc; % m^3 kmol^-1 
slst(1).Zc           = slst(1).pc*slst(1).vc/(R_univ*slst(1).Tc);
slst(1).coeffs       = R_univ * [4.078323210D+04, -8.009186040D+02, 8.214702010D+00,-1.269714457D-02, 1.753605076D-05,-1.202860270D-08, 3.368093490D-12, 2.682484665D+03, -3.043788844D+01];
slst(1).delta_1_CEOS = 1 + sqrt(2.); % (not needed here for PR EOS because u,w,delta_1,delta_2 are constants; check, e.g., tab.2 in doi: 10.1016/j.combustflame.2011.10.008)
slst(1).delta_2_CEOS = 1 - sqrt(2.); % (not needed here for PR EOS because u,w,delta_1,delta_2 are constants; check, e.g., tab.2 in doi: 10.1016/j.combustflame.2011.10.008)

% Component #2
slst(2).Name         = 'N2';
slst(2).M            = 28.014;           % kg kmol^-1
slst(2).omega        = 0.0372;           % -
slst(2).pc           = 3.3958e6;         % Pa 
slst(2).Tc           = 126.192;          % K 
slst(2).rhoc         = 11.1839;          % kmol m^-3
slst(2).vc           = 1./slst(2).rhoc;  % m^3 kmol^-1 
slst(2).Zc           = slst(2).pc*slst(2).vc/(R_univ*slst(2).Tc);
slst(2).coeffs       = R_univ * [2.210371497D+04, -3.818461820D+02, 6.082738360D+00, -8.530914410D-03, 1.384646189D-05, -9.625793620D-09, 2.519705809D-12, 7.108460860D+02, -1.076003744D+01];
slst(2).delta_1_CEOS = 1 + sqrt(2.);
slst(2).delta_2_CEOS = 1 - sqrt(2.);

k_ij = [ 0.000, 0.000 ...
       ; 0.000, 0.000 ]; % If available: set the binary interaction parameter

% initialize a parameter (only the a in a*alpha(T)), b parameter and c
% parameter (which is used in the alpha function of a*alpha(T))
for i=1:nc
    slst(i).a  = a_PR( R_univ, slst(i).Tc, slst(i).pc, slst(i).Zc );  
    slst(i).b  = b_PR( R_univ, slst(i).Tc, slst(i).pc, slst(i).Zc ); 
    slst(i).c  = c_PR( slst(i).omega, slst(i).Zc );  
end

% Solver parameters for fsolve
options=optimset('simplex','off'...
                ,'jacobian','off'...
                ,'LargeScale','on'...
                ,'MaxFunEvals',1000 ...
                ,'MaxNodes',2 ...
                ,'TolFun',1.0000e-16 ...
                ,'Tolx', 1.0e-10 ...
                ,'display','off'...
                ,'RelLineSrchBnd',0.05 ...
                ,'RelLineSrchBndDuration',20 ...
                ,'Algorithm','trust-region-reflective');    
               
% Calculate adiabatic mixing temperature
nz = 41; 
z1 = linspace(0,1,nz); 
z2 = 1 - z1; 

T1 = 270.;     % Temperature of comp. #1 (H2) before mixing
T2 = 118.;     % Temperature of comp. #2 (N2) before mixing
p  = 40.e5;    % Pressure

% Calculate EOS parameters for pure components
[aalpha_1, daalphadT_1, ddaalphadT_1, b_1, aalpha_jk_1] = mix_GCEOS(nc, [1.;0.], T1);
[aalpha_2, daalphadT_2, ddaalphadT_2, b_2, aalpha_jk_2] = mix_GCEOS(nc, [0.;1.], T2);

% For pure component 1 & 2: Calculate volume, internal energy, enthalpy,
% cv, cp, speed of sound, and partial molar volume/enthalpy 
[vol_1, u_1 , h_1, cv_1, cp_1, sound_1, dvdx_Tp_1, phase_1] ...
          = prop_GCEOS( u_EOS, w_EOS    ...
                      , aalpha_1, daalphadT_1, ddaalphadT_1, aalpha_jk_1 ...
                      , b_1, slst(1).coeffs, T1, p, 'Gibbs' );

[vol_2, u_2 , h_2, cv_2, cp_2, sound_2, dvdx_Tp_2, phase_2] ...
          = prop_GCEOS( u_EOS, w_EOS    ...
                      , aalpha_2, daalphadT_2, ddaalphadT_2, aalpha_jk_2 ...
                      , b_2, slst(2).coeffs, T2, p, 'Gibbs' );
                                 
for i=1:nz

    z = [z1(i),z2(i)];  % Overall mole fraction z 

    if i == 1 % Initial guess
        temp = z(1) * T1 + z(2) * T2;
    else
        % Do nothing. temp is the best guess we have.
    end
    
    hTarget = z(1) * h_1 +z(2) * h_2; % Target enthalpy
  
    eps = 1;
    nit = 0;
    
    converged = false;
    
    while nit < 60
        
        nit = nit + 1;
        
        % Calculate EOS parameter for mixture
        [aalpha_m, daalphadT_m, ddaalphadT_m, b_m, aalpha_jk_m] = mix_GCEOS(nc, z, temp);

        % Mix ideal reference state
        coeffs_m = zeros(size(slst(1).coeffs));
        for n=1:nc
            coeffs_m = coeffs_m + z(n) * slst(n).coeffs;
        end
    
         % Calculate enthalpy of mixture (here, our mixing rules apply)
        [vol_m, u_m , h_m, cv_m, cp_m, sound_m, dvdx_Tp_m, phase_m] ...
                  = prop_GCEOS( u_EOS, w_EOS    ...
                              , aalpha_m, daalphadT_m, ddaalphadT_m, aalpha_jk_m  ...
                              , b_m, coeffs_m, temp, p, 'Gibbs' );
        
        eps = abs((hTarget-h_m)/hTarget);
        if eps <= 1e-6
            converged = true;
            break
        else 
            % Update temperature
            alpha    = 1.E-01;
            delta_h  = (h_m - hTarget);
            dT       = - delta_h / cp_m; 
            C        = 1. / ( 1 + abs(dT * alpha) );
            temp     = temp + dT * C;      
        end             

    end;
  
    if not(converged) 
        error('z_1 = %8.4f | T = %6.3f | n = %4i | Resid = %12.6e \n',z(1),temp,nit,eps)
    end 

    M    = z(1) * slst(1).M + z(2) * slst(2).M; % Mixture molar mass 
    Y(1) = z(1) * slst(1).M / M; % Overall mass fraction comp. #1 
    Y(2) = z(2) * slst(2).M / M; % Overall mass fraction comp. #2

    T_F(i)  = temp;  % 'Frozen' adiabatic mixing temperature (T_F)
    
    vol_F(i)    = vol_m;     % Frozen adiabatic mixing volume (m^3/mol)
    vol_F_kg(i) = vol_m / M; % Frozen adiabatic mixing volume (m^3/kg)

    rho_F_kg_1(i) = Y(1) / vol_F_kg(i); % Frozen partial density comp. #1 (H2) 
    rho_F_kg_2(i) = Y(2) / vol_F_kg(i); % Frozen partial density comp. #2 (N2)
    
    % Check stability of phase 
    [stable, TPDmin, K_init] = solveTPD_SSI( nc, z, temp, p );
    %[stable, TPDmin, K_init] = solveTPD_BFGS( nc, z, temp, p ); 
    
    if not(stable) % Calculate equilibrium adiabatic mixing temperature (T_EQ)
        
            fprintf('==> Single phase NOT STABLE. \n');        
 
            T_init = temp; % Initial value from T_F solution
            HPn = @(T) objectiveHPN( T, z, hTarget, p ); % Create function handle for fsolve
            
            % Adjust temperature such that h = psi_v * h_v   + ( 1. - psi_v ) * h_l = hTarget
            [T_EQ(i),fval,exitflag] = fsolve(HPn, T_init, options); 

            % Error handling
            if not(any(exitflag==[1 2 3 4]))
                disp('NOT CONVERGED!')
                formatSpec = 'HPn NOT CONVERGED! exitflag = %u | Residuum = %12.6e \n';
                fprintf(formatSpec,exitflag,fval);
                stop
            end
            
            % Get composition of liquid and vapor phases and phase fraction psi_v
            % Phase fraction psi_v on molar basis := mol vapor / mol mixture
            [~, x, y, psi_mol(i), u_l, u_v, h_l, h_v, vol_l, vol_v] ...
                                    = objectiveHPN( T_EQ(i), z, hTarget, p );
            
            vol_EQ(i) = psi_mol(i) * vol_v + ( 1. - psi_mol(i) ) * vol_l;   
   
            M_l = x(1) * slst(1).M + x(2) * slst(2).M; % Mixture molar mass in liquid phase
            M_v = y(1) * slst(1).M + y(2) * slst(2).M; % Mixture molar mass in vapor phase
            % 
            vol_l_kg = vol_l / M_l; % Specific molar volume liquid phase (m^3 kg^-1)
            vol_v_kg = vol_v / M_v; % Specific molar volume vapor phase (m^3 kg^-1)
            % 
            w_l(1) = x(1) * slst(1).M / M_l; % Mass fraction of comp. #1 (H2) in the liquid phase
            w_v(1) = y(1) * slst(1).M / M_v; % Mass fraction of comp. #1 (H2) in the vapor phase
            w_l(2) = x(2) * slst(2).M / M_l; % Mass fraction of comp. #2 (N2) in the liquid phase
            w_v(2) = y(2) * slst(2).M / M_v; % Mass fraction of comp. #2 (N2) in the vapor phase
            % 
            rho_l_kg = 1. / vol_l_kg; % Density (kg m^-3) of liquid phase
            rho_v_kg = 1. / vol_v_kg; % Density (kg m^-3) of vapor phase 
            % 
            rho_l_kg_1(i)  = rho_l_kg * w_l(1); % Partial density liquid phase of comp. #1 (H2)  
            rho_v_kg_1(i)  = rho_v_kg * w_v(1); % Partial density vapor phase of comp. #1 (H2) 
 
            rho_l_kg_2(i)  = rho_l_kg * w_l(2); % Partial density liquid phase of comp. #2 (N2) 
            rho_v_kg_2(i)  = rho_v_kg * w_v(2); % Partial density vapor phase of comp. #2 (N2) 
            
            psi_mass   = psi_mol(i) * M_v / M; % Phase fraction psi_v on mass basis := kg vapor / kg mixture

            vol_EQ_kg(i) = psi_mass * vol_v_kg + ( 1. - psi_mass ) * vol_l_kg;
            
            rho_EQ_kg_1(i)  = 1. / vol_EQ_kg(i) * Y(1); % Overall partial equilibrium density comp. #1 (kg m^-3)
            rho_EQ_mol_1(i) = 1. / vol_EQ(i) * z(1); % Overall partial equilibrium density comp. #1 (mol m^-3)
            
            rho_EQ_kg_2(i)  = 1. / vol_EQ_kg(i) * Y(2);  % Overall partial equilibrium density comp. #2 (kg m^-3)
            rho_EQ_mol_2(i) = 1. / vol_EQ(i) * z(2); % Overall partial equilibrium density comp. #2 (mol m^-3)
                    
    else
        
        fprintf('==> Single phase STABLE. \n');
        
        % Frozen and equilibrium approach collapse outside of two-phase region 
        T_EQ(i)        = temp;  
        vol_EQ(i)      = vol_m;     
        vol_EQ_kg(i)   = vol_m / M; 
        rho_EQ_kg_1(i) = rho_F_kg_1(i);
        rho_EQ_kg_2(i) = rho_F_kg_2(i);  
        
        % There are no liq. and vap. phase densties
        rho_l_kg_1(i)  = -1.; 
        rho_v_kg_1(i)  = -1.;
        rho_l_kg_2(i)  = -1.; 
        rho_v_kg_2(i)  = -1.;
              
    end
             
end;


%% Do some post processing:
font_size = 16;
fcount    = 0; % Counter

% PLOT #1 - All partial densities of comp. #1 (H2): 
fcount  = fcount + 1;
figure(fcount)
fig(1) = plot(z1,rho_F_kg_1,'k-o',...
        'LineWidth',1,...
        'MarkerSize',5,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','w');
hold on   
fig(2) = plot(z1,rho_EQ_kg_1,'k-o',...
        'LineWidth',1,...
        'MarkerSize',5,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','k');
hold on  
fig(3) = plot(z1,rho_l_kg_1,'o',...
        'LineWidth',1,...
        'MarkerSize',5,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','b');
hold on 
fig(4) = plot(z1,rho_v_kg_1,'o',...
        'LineWidth',1,...
        'MarkerSize',5,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','r');
set(gca,'FontSize',font_size)    
xlim([0 1]);
ylim([0 6]); 
legend(fig,{'Frozen','Equilibrium','Liquid','Vapor'},'Location','southeast');
title(['Hydrogen partial densities'], 'FontSize', font_size)
xlabel('z_{H_2} [mol/mol]')

% PLOT #2 - All partial densities of comp. #1 (N2): 
fcount  = fcount + 1;
figure(fcount)
fig(1) = plot(z1,rho_F_kg_2,'k-o',...
        'LineWidth',1,...
        'MarkerSize',5,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','w');
hold on 
fig(2) = plot(z1,rho_EQ_kg_2,'k-o',...
        'LineWidth',1,...
        'MarkerSize',5,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','k');
hold on                 
fig(3) = plot(z1,rho_l_kg_2,'o',...
        'LineWidth',1,...
        'MarkerSize',5,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','b');
hold on 
fig(4) = plot(z1,rho_v_kg_2,'o',...
        'LineWidth',1,...
        'MarkerSize',5,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','r');
set(gca,'FontSize',font_size)    
xlim([0 1]);
ylim([0 600]); 
legend(fig,{'Frozen','Equilibrium','Liquid','Vapor'},'Location','northeast');
title(['Nitrogen partial densities'], 'FontSize', font_size)
xlabel('z_{H_2} [mol/mol]')

%% Do some more post-processing - TPD and Gibbs Free Energy of Mixing 
if plot_TPD

    nw = 51;
    small = 1E-06;
    w_trial  = linspace(0.+small,1.-small,nw); % Trial phase composition w

    tpd     = zeros(nw,1); % Tangent Plane Distance function
    dgmix   = zeros(nw,1); % Gibbs free energy of mixing
    pminTPD = cell(0,0);

    for i=1:nz

        z0 = [z1(i),z2(i)]; % Composition to be tested (same vector as used for adiab. mixing model calculation)

        dgmixl = []; % Append Gibbs free energy of mixing if phase is identified as liquid
        dgmixv = []; % Append Gibbs free energy of mixing if phase is identified as vapor

        pzl = []; % Append trial phase composition for which dgmix is identified as liquid
        pzv = []; % Append trial phase composition for which dgmix is identified as vapor

        for k=1:nw

            tpd(k) = evalTPD( nc, [w_trial(k), 1-w_trial(k)], z0, T_F(i), p ); % Evaluate TPD function
           [dgmix(k), phase] = evaldgmix( nc, [w_trial(k), 1-w_trial(k)], T_F(i), p ); % Evaluate Gibbs free energy of mixing

           % Link dgmix to liq. and vap. phase to use colors in the final plot
           if strcmp(phase,'Liq')
               pzl(end+1)    = w_trial(k);
               dgmixl(end+1) = dgmix(k);
           elseif strcmp(phase,'Vap')
               pzv(end+1)    = w_trial(k);
               dgmixv(end+1) = dgmix(k);
           end     

        end

        if z1(i) > 0.05 && z1(i) < 0.40  % Plot only the region interest  

            fcount = fcount + 1;
            pminTPD{end+1} = [z1(i), T_F(i)];
            figure(fcount)
            subplot(2,1,1)  
            % Plot TPD function
            fig = plot(w_trial,tpd,'k-o',...
                    'LineWidth',1,...
                    'MarkerSize',5,...
                    'MarkerEdgeColor','k',...
                    'MarkerFaceColor','b');               
            hold on 
            % Plot single point indicating the overall composition which is tested
            plot(z0(1),0.,'o',...
                    'LineWidth',1,...
                    'MarkerSize',8,...
                    'MarkerEdgeColor','k',...
                    'MarkerFaceColor','w');
            xlim([0 1]);
            ylim([-0.08 0.08]);
            title(['Tangent plane distance (tpd) @ T = ' num2str(T_F(i),'%3.2f\n') ' | z_{H_2} = ' num2str(z0(1),'%3.2f\n')], 'FontSize', font_size)
            set(gca,'FontSize', font_size)
            xlabel('w_{H_2} [mol/mol] (trial phase comp.)')


            subplot(2,1,2) 
            % Plot Gibbs energy of mixing (identified as liq.)
            fig(1) = plot(pzl,dgmixl,'k-o',...
                    'LineWidth',1,...
                    'MarkerSize',5,...
                    'MarkerEdgeColor','k',...
                    'MarkerFaceColor','b');
            hold on
            % Plot Gibbs energy of mixing (identified as vap.)
            fig(2) = plot(pzv,dgmixv,'k-o',...
                    'LineWidth',1,...
                    'MarkerSize',5,...
                    'MarkerEdgeColor','k',...
                    'MarkerFaceColor','r');
            hold on 
            % Plot single point indicating the overall composition which is tested
            work = interp1(w_trial,dgmix,z0(1));
            plot(z0(1),work,'o',...
                    'LineWidth',1,...
                    'MarkerSize',8,...
                    'MarkerEdgeColor','k',...
                    'MarkerFaceColor','w');
            xlim([0 1]);
            ylim([-0.5 0.])
            legend(fig,{'Liquid','Vapor'},'Location','southwest');
            title(['Gibbs free energy of mixing @ T = ' num2str(T_F(i),'%3.2f\n')], 'FontSize', font_size)
            set(gca,'FontSize', font_size)
            xlabel('z_{H_2} [mol/mol]')


        end    
    end
end 


%% Calculate Txy diagram (brute force method) and plot adiabatic mixing temp.

nT   = 191;  % number of T steps for VLE calculation 
T    = linspace(90,130,nT); 

% Create an initial guess for the lowest temperature
[stable, TPDmin, K_init] = solveTPD_SSI( nc, [0.5, 0.5], T(1), p );
[x, y, psi_mol, fval, success, n_it] = solveTPN( nc, [0.5, 0.5], T(1), p, K_init );

xy = [x(1), y(1)];
for m=1:nT
    % Function handle
    VLE = @(xy) objectiveTPBinary(xy, T(m), p);
    % Solve VLE with fsolve
    [xy,fval,exitflag] = fsolve(VLE,xy,options);
    % Error handling
    if not(any(exitflag==[1 2 3 4]))
        error('fsolve did not converge! exitflag = %u | residuum = %12.6e \n',exitflag,fval)
    end   
    
    if abs(xy(1)-xy(2)) <= 1.E-16
       break % Trivial solution (x=y)
    end 
    
    x(m)   = xy(1); % Mole fraction of comp. #1 in liq. phase
    y(m)   = xy(2); % mole fraction of comp. #1 in vap. phase
    Txy(m) = T(m); % Remember the temperature that belongs to x & y
    formatSpec = 'T = %6.3f | p = %8.4e | x = %6.3f (liq.) | y = %6.3f (vap.)\n';
    fprintf(formatSpec,T(m),p,xy(1),xy(2));   
end 

figure(fcount+1)
% Plot bubble-point line:
fig(1) = plot(x,Txy,'b-',...
                'LineWidth',1);
hold on
% Plot dew-point line:
fig(2) = plot(y,Txy,'r-',...
                'LineWidth',1);
hold on
% Plot frozen adiab. mixture line:
fig(3) = plot(z1,T_F,'k-o',...
        'LineWidth',1,...
        'MarkerSize',5,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','w');
hold on
% Plot equilibrium adiab. mixing line:
fig(4) = plot(z1,T_EQ,'k-o',...
        'LineWidth',1,...
        'MarkerSize',5,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','k');
xlim([0 0.8]);
ylim([100 140])   
set(gca,'FontSize',font_size)
legend('Liq','Vap','T_F','T_{EQ}','Location','northeast')
title(['Frozen/Equilibrium Adiabatic Mixing Temperature'], 'FontSize', font_size)
xlabel('x_{H_2},y_{H_2},z_{H_2} [mol/mol]')

