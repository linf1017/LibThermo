function [alpha, dalphadT, ddalphadT] = alpha( T, Tc_i, c_i )
% ALPHA  Calculate alpha function for Peng-Robinson EOS
%
% Inputs: 
%    T         - Temperature
%
% Outputs:
%    alpha     - alpha(T) function
%    dalphadT  - 1st partial derivative of alpha(T) with respect to T  
%    ddalphadT - 2nd partial derivative of alpha(T) with respect to T
%
% Author: Jan Matheis, Technical University Munich, jan.matheis@gmail.com

global alpha_function

if strcmp(alpha_function,'DEF')

    work      =   1. + c_i * ( 1. - sqrt(T / Tc_i) ); 
    alpha     =   work^2;
    dalphadT  = - c_i * work / sqrt( Tc_i * T );
    ddalphadT =   c_i * ( c_i + 1. ) / ( 2. * sqrt( T*T*T * Tc_i) );

else
    
    error('Unkown alpha function %s.',alpha_function)

end

end

