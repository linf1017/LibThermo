function [stable, TPDmin, K_init_min] = solveTPD_SSI( nc, z, temp, press )
% SOLVETPD_SSI   Perform TPD analysis using the successive substitution method (SSI)
%
% Inputs: 
%    nc           - Number of components
%    w_trial(nc)  - Trial phase composition 
%    z_test(nc)   - Composition z that is tested  
%    temp, press  - Temperature and pressure
%
% Outputs:
%    stable      - Stable? (true or false)
%    TPDmin      - Mimimum value of non-dimensional tpd function 
%    K_init_min  - K-factor which led to convergence towards TPDmin  
%
% Authors: 
%    Jan Matheis, Technical University Munich, jan.matheis@gmail.com
%
% Please cite this work as: 
%    J. Matheis and S. Hickel (2017), Multi-component vapor-liquid equilibrium 
%    model for LES of high-pressure fuel injection and application to ECN Spray A
%    Int. J. Multiph. Flow, doi: 10.1016/j.ijmultiphaseflow.2017.11.001
%
% Literature:
%   REF1: Michelsen (1982) The isothermal flash problem. Part I. Stability
%   REF2: Hoteit & Firoozabadi (2006) Simple phase stability testing algorithm in the reduction method
%   REF3: Li & Firoozabadi (2012) General Strategy for Stability Testing and Phase-Split Calculation in Two and Three Phases
%   REF4: Michelsen & Mollerup Thermodynamic Models Fundamentals and Computational Aspects


global R_univ
global u_EOS w_EOS
global slst

print_info        = false;
be_negative       = -1.E-8; % Define what is negative numerically
be_pure_substance = 1. - 1.E-8; % Define what we mean by pure substance
return_if_negative = true; % return immediately if negativity is detected 
                           % discard other trial phase compositions
                           
% No TPD analysis for "pure" substances
if any(z >= be_pure_substance) 
    TPDmin     = 10.;
    stable     = true;
    K_init_min = 0.;
    return 
end

ntrial    = 4; % Number of trial compostions we will use to find the 
               % global minium of the TPD function. Note that this
               % approach does not guarantee to locate the global 
               % minumum of the TPD.
               
tol       = 1.E-9; % Residuum for convergence as in REF2
iter_max  = 10000; % Maximum number of iterations
y_sp      = zeros(ntrial,nc); % Stationary points of TPD

% Get mixed EOS params of assumed single phase z
[aalpha_z, ~, ~, b_z, aalpha_jk_z, ~] = mix_GCEOS( nc, z, temp );

% Get compressibility factor of assumed single phase z 
[vol_z, phase_z] = vol_GCEOS( u_EOS, w_EOS...
                            , aalpha_z, b_z...
                            , temp, press, 'Gibbs' );
Z_z = vol_z(1) * press / ( R_univ * temp );

% Calculate fugacity coeffcients of assumed single phase z
for i=1:nc; 
   lnphi_z(i) = lnphi_GCEOS( u_EOS, w_EOS ...
                           , slst(i).b, b_z ...
                           , aalpha_jk_z(i), aalpha_z...
                           , Z_z, temp, press );
   d(i)       = log( z(i) ) + lnphi_z(i); % Eqn. 3 in REF2
end

% REF4, p. 270 : "The procedure suggested here for stability analysis is 
% very conservative in the sense that it attempts to minimise the risk of 
% missing a two-phase solution. In most cases, converging stability analysis 
% twice, with initial estimates from both eqns. (49) and (50) represents 
% wasted effort, as at least one of these estimates is very likely to 
% converge to the trivial solution. If a phase identification for the 
% feed can be made with confidence (as 'liquid-like' or as 'vapour-like'), 
% a one-sided stability analysis is adequate. Such a phase identification 
% is, however, difficult to perform in the critical region, and if emphasis 
% is on safety the two-sided approach is the only 'guarantee' against 
% misinterpreting the phase distribution."
% Depending on the result of phase_z we order Y_init(1:2,:) and Y_init(3:4,:)  
% if phase_z == Liq -> trial phase composition should be Vap
% if phase_z == Vap -> trial phase composition should be Liq
% See also Eq. A2. in REF2
% We use the Wilson equation to generate trial phase compositions
for i=1:nc
    K(i) = slst(i).pc/press*exp(5.37*(1+slst(i).omega)*(1-slst(i).Tc/temp));
end 

if strcmp(phase_z,'Liq')
    Y_init(1,:) = z .* K;          % If trial phase is vap (-> tested phase z is liquid)
    Y_init(2,:) = z .* K.^(1./3.); % If trial phase is vap (-> tested phase z is liquid)  
    Y_init(3,:) = z ./ K;          % If trial phase is liq (-> tested phase z is vapor)
    Y_init(4,:) = z ./ K.^(1./3.); % If trial phase is liq (-> tested phase z is vapor)
else 
    Y_init(1,:) = z ./ K;          % If trial phase is liq (-> tested phase z is vapor)
    Y_init(2,:) = z ./ K.^(1./3.); % If trial phase is liq (-> tested phase z is vapor)
    Y_init(3,:) = z .* K;          % If trial phase is vap (-> tested phase z is liquid)
    Y_init(4,:) = z .* K.^(1./3.); % If trial phase is vap (-> tested phase z is liquid)   
end 

% See App. A in REF2 for details on the SSI-Algorithm (here without Newton)
for k=1:ntrial

    for i=1:nc
        Y(i) = Y_init(k,i);
    end 

    break_eps = false;
    for iter = 1:iter_max

        y_trial = Y/sum(Y);

        [aalpha_y, ~, ~, b_y, aalpha_jk_y, ~] = mix_GCEOS( nc, y_trial, temp );

        [vol_y, phase_y] = vol_GCEOS( u_EOS, w_EOS ...
                                    , aalpha_y, b_y ...
                                    , temp, press, 'Gibbs' );
        Z_y = vol_y(1) * press / ( R_univ * temp );

        for i=1:nc
            lnphi_y = lnphi_GCEOS( u_EOS, w_EOS ...
                                 , slst(i).b, b_y ...
                                 , aalpha_jk_y(i), aalpha_y ...
                                 , Z_y, temp, press );    
            Yn(i) = exp( d(i) - lnphi_y );
        end
        
        dY  = Yn - Y;
        eps = sqrt(abs(dY*dY'));
        Y   = Yn;

        if eps <= tol
            
            y_sp(k,:) = Y/sum(Y); % Stationary points of TPD and TPD_star function 

            
            TPD_star(k) = 1. - sum(Y); % This is only true at the staionary points, cf. Eq. 17 in REF1
                                       % otherwise:  TPD_star = 1 + sum( Y(i) * ( lnphi_y + log(Y(i)) - d(i) - 1 ) )            
            break_eps = true; 
            break % Move on to the next trial phase composition because eps <= tol                       
        
        end % eps < tol
        
    end % Maximum iterations

    if print_info
        formatSpec = '  TPD: Phase(z) = %s | z(1) = %8.4f | z(2) = %8.4f \n';
        fprintf(formatSpec,phase_z,z(1),z(2)); 
        formatSpec = '  TPD: Phase(y) = %s | y(1) = %8.4f | y(2) = %8.4f | k = %1i | nit = %4i | Resid = %+8.4e \n';
        fprintf(formatSpec,phase_y,y_sp(k,1),y_sp(k,2),k,iter,eps); 
    end     
   
    % Leave solveTPD_SSI here to speed up things.
    % Take care: We might miss the true TPD minimum if we leave
    % the TPD test here. We might have found a minimum, but not the
    % global minimum.
    if return_if_negative && break_eps % Make sure TPD_star is available
        if TPD_star(k) < be_negative
            break % Leave trial loop here
        end % Return immediately when negative TPD detected
    end
           
end % Trials

% If SSI does not converge we can still calculate TPD_star (we must now use to original formula)
% If TPD_star negative: phase is not stable
% If TPD_star is not negative: 
if exist('TPD_star','var') ~= 1
    warning('solveTPD_SSI did not converge such that eps <= tol')
    TPD_star = 1;
    for i=1:nc
        lnphi_y = lnphi_GCEOS( u_EOS, w_EOS ...
                             , slst(i).b, b_y ...
                             , aalpha_jk_y(i), aalpha_y ...
                             , Z_y, temp, press );    
        TPD_star = TPD_star + Y(i) * ( lnphi_y + log( Y(i) ) - d(i) - 1 );
    end 
end


TPDmin = min(TPD_star);
if TPDmin < be_negative % Eq. 4 in REF2
    % Return the K-factor which led to lowest TPD value as initial guess
    % for the TPn flash calculation. 
    % Recommended by REF3, Phase-Split Calculation, p. 1098
    
    stable = false; 
    
    [TPDmin, iTPDmin] = min(TPD_star);

    % Back to K-Factors
    if strcmp(phase_z,'Liq') 
        switch iTPDmin
            case 1
               K_init_min = Y_init(iTPDmin,:) ./ z(:)'; 
            case 2
               K_init_min = (Y_init(iTPDmin,:) ./ z(:)').^3;
            case 3
               K_init_min =  z(:)' ./ Y_init(iTPDmin,:); 
            case 4
               K_init_min = (z(:)' ./ Y_init(iTPDmin,:)).^3;
        end
    else 
        switch iTPDmin
            case 1
               K_init_min =  z(:)' ./ Y_init(iTPDmin,:); 
            case 2
               K_init_min = (z(:)' ./ Y_init(iTPDmin,:)).^3;
            case 3
               K_init_min = Y_init(iTPDmin,:) ./ z(:)'; 
            case 4
               K_init_min = (Y_init(iTPDmin,:) ./ z(:)').^3;
        end
    end 
        
else
           
    stable = true;
    K_init_min = 0.;
    
end 
