function lnphi_k = lnphi_GCEOS( u_EOS, w_EOS, b_k, b, aalpha_jk, aalpha, Z, temp, press )
% LNPHI_GCEOS   Get fugacity coefficient for cubic EOS 
%
% Inputs: 
%    u_EOS, w_EOS - Parameter for generalized cubic EOS
%    b_k          - co-volume of component k
%    b            - co-volume of mixture
%    aalpha_jk    - summe_{j=1}^{N_c}(z_j*a_jk*alpha_jk) for component k
%    aalpha       - a*alpha of mixture 
%    Z            - compressibility factor of mixture
%    temp, press  - temperature and pressure
%
% Outputs:
%    lnphi_k      - Fugacity coefficient
%
% Author: 
%    Jan Matheis, Technical University Munich, jan.matheis@gmail.com
%
% Literature:
%    REF1: "Introductory Chemical Engineering Thermodynamics", J. Richard Elliott, Carl T. Lira
%    See Eqn. 15.34 (for a binary 15.18/15.19) in REF1
%    Remember that: 
%    ln(f_i^L/(x_i*p)) = ln(phi_i^L) or ln(f_i^V/(y_i*p)) = ln(phi_i^V)
%    At equlibrium:      f_i^L = f_i^V which is equivalent to 
%            p * x_i * phi_i^L = p * y_i * phi_i^V 
%   
%    IMPORTANT: THIS FORMULA IS NOT VALID FOR RKPR EOS (see, e.g., doi: 10.1016/j.combustflame.2011.10.008) 

global R_univ
global EOS

if strcmp(EOS,'PR') || strcmp(EOS,'SRK')
    % OK: Move on.
else
    error(['Make sure the derivative works for your EOS.' ...
           'For RK-PR you need to take the composition dependency' ...
           'of u & w / delta_1 & delta_2 into account.'])
end

% For PR and SRK uEOSwEOS will never change
uEOSwEOS = sqrt( u_EOS^2 - 4. * w_EOS ); 

% Short cuts
factA = ( aalpha * press ) / ( R_univ * temp )^2;
factB = ( b * press ) / ( R_univ * temp );

lnphi_k = b_k / b * ( Z - 1 ) ...
        - log( Z - factB ) ...
        - factA / ( factB * uEOSwEOS ) ...
        * ( 2 * aalpha_jk / aalpha - b_k / b  ) ...
        * log( ( 2 * Z + factB * ( u_EOS + uEOSwEOS ) ) / ( 2 * Z + factB * ( u_EOS - uEOSwEOS ) ) );

end
