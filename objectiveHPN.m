function [F, x0, y0, psi_v, u_l, u_v, h_l, h_v, vol_l, vol_v, dvdx_Tp_l, dvdx_Tp_v, sound_l, sound_v ] = objectiveHPN( temp, z, h_fix, p_fix )
% OBJECTIVEHPN  Objective function (not the solution) for equilibrium  
%               calculation at specified enthalpy, pressure and overall  
%               composition. fsolve can do the job. 
%
% Inputs: 
%    temp       - Temperature
%    z(nc)      - Overall composition  
%    h_fix      - Target enthalpy
%    p_target   - Target pressure
%
% Outputs:
%    F                - Residuum objective function
%    x0(nc)           - Liquid phase comp.
%    y0(nc)           - Vapor phase comp.
%    psi_v            - Molar vapor fraction
%    u_l, u_v         - Liquid and vapor internal energy
%    h_l, h_v         - Liquid and vapor enthalpy
%    dvdx_Tp_l(nc)    - Vector of partial molar volumes d(nV)dn_i|_{T,p,i \neq j} liquid phase
%    dvdx_Tp_v(nc)    - Vector of partial molar volumes d(nV)dn_i|_{T,p,i \neq j} vapor phase
%    sound_l, sound_v - Liquid and vapor speed of sound 
%
% Author: 
%    Jan Matheis, Technical University Munich, jan.matheis@gmail.com

global u_EOS w_EOS
global slst
global nc 
global K_init

print_info = false;

% Inner loop: Equilibrium calculation at specified temperature T, pressure p and overall composition z.
[x0, y0, psi_v, ~, success, ~] = solveTPN( nc, z, temp, p_fix, K_init );

if not(success)
    if print_info
        % Not necessarily a problem
        disp('solveTPN did not converge.')
    end
else
    K_init = y0 ./ x0;
end

% Get mixture EOS params for both phases
[aalpha_l, daalphadT_l, ddaalphadT_l, b_l, aalpha_jk_l] = mix_GCEOS(nc, x0, temp);
[aalpha_v, daalphadT_v, ddaalphadT_v, b_v, aalpha_jk_v] = mix_GCEOS(nc, y0, temp);

% Mix ideal reference state for both phases
coeffs_l = zeros(size(slst(1).coeffs));
coeffs_v = zeros(size(slst(1).coeffs));
for i=1:nc
    coeffs_l = coeffs_l + x0(i) * slst(i).coeffs;
    coeffs_v = coeffs_v + y0(i) * slst(i).coeffs;    
end

% Get enthalpy of both phase (this could have been done in TPn_FLASH as well)
[vol_l, u_l , h_l, cv_l, cp_l, sound_l, dvdx_Tp_l, phase_l] ...
          = prop_GCEOS( u_EOS, w_EOS    ...
                      , aalpha_l, daalphadT_l, ddaalphadT_l, aalpha_jk_l  ...
                      , b_l, coeffs_l, temp, p_fix, 'Liq' );
[vol_v, u_v , h_v, cv_v, cp_v, sound_v, dvdx_Tp_v, phase_v] ...
          = prop_GCEOS( u_EOS, w_EOS    ...
                      , aalpha_v, daalphadT_v, ddaalphadT_v, aalpha_jk_v  ...
                      , b_v, coeffs_v, temp, p_fix, 'Vap' );

h = psi_v * h_v   + ( 1. - psi_v ) * h_l; % Mixture enthalpy (vapor fraction on a molar basis)

F = (h_fix - h) / h_fix; % Objective function to be minimized

end