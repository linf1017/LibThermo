function [F, J] = objectiveTPN( v, z, temp, press, v_ref )
% OBJECTIVETPN  Objective function (not the solution) for equilibrium  
%               calculation at specified temperature, pressure and overall  
%               composition. fsolve can do the job. 
%
% Inputs: 
%    v(nc)      - Molar flow (normalized)
%                 v_i = psi_v * y_i
%                     with psi_v = molar vapor fraction 
%                            y_i = mole fraction of component i in vapor phase 
%    temp       - Temperature
%    press      - Pressure
%    v_ref(nc) -  Normalization of molar flow  
%
% Outputs:
%    F(nc)      - Residuum objective functions
%    J(nc,nc)   - Analytical Jacobian
%
% See also: SOLVETPN
%
% Author: 
%    Jan Matheis, Technical University Munich, jan.matheis@gmail.com
%
% Literature:
%    REF1: Michelsen (1982) The isothermal flash problem. Part I. Stability
%    REF2: Michelsen & Mollerup (2007) Thermodynamic Models: Fundamentals & Computational Aspects
%

global R_univ 
global u_EOS w_EOS
global slst
global nc


v(1) = v(1) * v_ref(1); % Re-scaling of molar flows
v(2) = v(2) * v_ref(2);

psi_v = sum(v); % Calculate molar vapor fraction

% Calculate liquid and vapor phase compositions
for i=1:nc;
    y(i) = v(i) / psi_v;
    x(i) = ( z(i) - v(i) ) / ( 1 - psi_v );
end  

% Get mixed EOS params for liq. and vap. phase 
[aalpha_l, ~, ~, b_l, aalpha_jk_l, aalpha_jk] = mix_GCEOS( nc, x, temp ); % aalpha_jk(i,j) = a(i,j) * alpha(i,j) 
[aalpha_v, ~, ~, b_v, aalpha_jk_v, ~        ] = mix_GCEOS( nc, y, temp ); % -> Does not depend on x and y 

% Get liq. and vap. volume / compressibility factor
% Remark: Passing 'Vap' to vol_GCEOS may exclude the creation of LLEs
[vol_v, ~] = vol_GCEOS( u_EOS, w_EOS, aalpha_v, b_v, temp, press, 'Vap' );
Z_v = vol_v(1) * press / ( R_univ * temp );
%
[vol_l, ~]= vol_GCEOS( u_EOS, w_EOS, aalpha_l, b_l, temp, press, 'Liq' );
Z_l = vol_l(1) * press / ( R_univ * temp );

% Calculate fugacity coeffcients and derivatives
lnphi_v = zeros(1,nc); % Logarithm of fugacity coeffcient
lnphi_l = zeros(1,nc); 
dlnphidn_l = zeros(nc,nc); % Derivative of logarithm of fug. coeff. with respect
dlnphidn_v = zeros(nc,nc); % to molenumber n_i in the corresponding phase

for i=1:nc
    lnphi_v(i) = lnphi_GCEOS( u_EOS, w_EOS, slst(i).b, b_v, aalpha_jk_v(i), aalpha_v, Z_v, temp, press );
    lnphi_l(i) = lnphi_GCEOS( u_EOS, w_EOS, slst(i).b, b_l, aalpha_jk_l(i), aalpha_l, Z_l, temp, press );
    for j=1:nc
        dlnphidn_v(i,j) = dlnphidn_GCEOS( u_EOS, w_EOS, slst(i).b, slst(j).b, b_v, aalpha_jk_v(i), aalpha_jk_v(j), aalpha_jk(i,j), aalpha_v, Z_v, temp, press );  
        dlnphidn_l(i,j) = dlnphidn_GCEOS( u_EOS, w_EOS, slst(i).b, slst(j).b, b_l, aalpha_jk_l(i), aalpha_jk_l(j), aalpha_jk(i,j), aalpha_l, Z_l, temp, press );  
    end
end

% Assembly analytical Jacobian matrix J 
kronDel = @(j, k) j==k;
J = zeros(nc,nc);
for i=1:nc; 
    for j=1:nc        
        work_v = 0.;
        work_l = 0.;
        for k=1:nc
            work_v = work_v + dlnphidn_v(i,k) * ( kronDel(j,k) - y(k) );
            work_l = work_l + dlnphidn_l(i,k) * ( x(k) - kronDel(j,k) );
        end         
        J(i,j) = 1. / ( psi_v * ( 1. - psi_v ) ) ...
               * ( z(i) / ( x(i) * y(i) ) * kronDel(i,j) - 1. ...
               + ( 1. - psi_v ) * work_v - psi_v * work_l );
    end
end

% Scaling of Jacobian
J(1,1)  = J(1,1) * v_ref(1);
J(1,2)  = J(1,2) * v_ref(2);

J(2,1)  = J(2,1) * v_ref(1);
J(2,2)  = J(2,2) * v_ref(2);

% Objective function to be minimized
F = zeros(1,nc);
for i=1:nc; 
    F(i) = lnphi_v(i) - lnphi_l(i) + log( ( v(i) * ( 1. - psi_v ) ) ...
                                        / ( psi_v * ( z(i) - v(i) ) ) );
end 

return


