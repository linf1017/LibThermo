% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% Author: Jan Matheis, Technical University Munich, jan.matheis@gmail.com
% Info: Calculate frozen and equilibrium mixing temperature for the binary 
%       sytem Nitrogen (N2) and n-Dodecane (C12H26). The main buidling 
%       blocks are  (i) cubic equations of state (ii) thermodynanmic  
%       stability analysis via TPD analysis and (iii) vapor-liquid  
%       equilibrium calculations. 
% Please cite this work as: 
% J. Matheis and S. Hickel (2017), Multi-component vapor-liquid equilibrium 
% model for LES of high-pressure fuel injection and application to ECN Spray A
% Int. J. Multiph. Flow, doi: 10.1016/j.ijmultiphaseflow.2017.11.001
% Please see for changes under https://gitlab.com/jmatheis/LibThermo
% Tested with MATLAB R2015a 64-bit (maci64).
% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% Nomenclature:
% Y =: overall mass fraction
% z =: overall mole fraction
% x =: liquid phase composition on a molar basis
% y =: vapor phase composition on a molar basis
% w =: trial phase composition on a molar basis of TPD test (or w_trial) 
% ... 
% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

clear all
%close all
clc

global R_univ EOS u_EOS w_EOS delta_1 delta_2
global nc
global slst k_ij
global crule 
global alpha_function
global K_init
 
R_univ  = 8314.472;  % J kmol^-1 K^-1
nc      = 2;    % Number of components
EOS     = 'PR'; % Which EOS do we solve?
                % Options: PR  =: Peng-Robinson

if strcmp(EOS,'PR') % Peng-Robinson EOS
    delta_1 =  1 + sqrt(2.); 
    delta_2 =  1 - sqrt(2.);  
    u_EOS   =  2; % = delta_1 + delta_2
    w_EOS   = -1; % = delta_1 * delta_2
else 
    error('Please implement %s on your own.',EOS)
end

crule = 'CRITICAL'; % Which combination rule do we use?
alpha_function = 'DEF';

slst(1).Name         = 'N2';
slst(1).M            = 28.014;           % kg kmol^-1
slst(1).omega        = 0.0372;           % -
slst(1).pc           = 3.3958e6;         % Pa 
slst(1).Tc           = 126.192;          % K 
slst(1).rhoc         = 11.1839;          % kmol m^-3
slst(1).vc           = 1./slst(1).rhoc;  % m^3 kmol^-1 
slst(1).Zc           = slst(1).pc*slst(1).vc/(R_univ*slst(1).Tc);
slst(1).coeffs       = R_univ * [2.210371497D+04, -3.818461820D+02, 6.082738360D+00, -8.530914410D-03, 1.384646189D-05, -9.625793620D-09, 2.519705809D-12, 7.108460860D+02, -1.076003744D+01];
slst(1).delta_1_CEOS = 1 + sqrt(2.); % (not needed here as species specific paramter for PR EOS because u,w,delta_1,delta_2 are constants; check, e.g., tab.2 in doi: 10.1016/j.combustflame.2011.10.008)
slst(1).delta_2_CEOS = 1 - sqrt(2.); % (not needed here as species specific paramter for PR EOS because u,w,delta_1,delta_2 are constants; check, e.g., tab.2 in doi: 10.1016/j.combustflame.2011.10.008)

slst(2).Name         = 'C12H26';
slst(2).M            = 170.3348;         % kg kmol^-1
slst(2).omega        = 0.5764;           % -
slst(2).pc           = 1.82e6;           % Pa 
slst(2).Tc           = 658.0;            % K 
slst(2).rhoc         = 1.330;            % kmol m^-3
slst(2).vc           = 1./slst(2).rhoc;  % m^3 kmol^-1  
slst(2).Zc           = slst(2).pc*slst(2).vc/(R_univ*slst(2).Tc);
slst(2).coeffs       = R_univ * [-1.583387502D+07, 2.318455011D+05, -1.301084838D+03, 3.729278890D+00, -5.271624900D-03, 3.786419430D-06, -1.083032706D-09, -1.147286089D+06, 7.267799460D+03];
slst(2).delta_1_CEOS = 1 + sqrt(2.); % (not needed here as species specific paramter for PR EOS because u,w,delta_1,delta_2 are constants; check, e.g., tab.2 in doi: 10.1016/j.combustflame.2011.10.008)
slst(2).delta_2_CEOS = 1 - sqrt(2.); % (not needed here as species specific paramter for PR EOS because u,w,delta_1,delta_2 are constants; check, e.g., tab.2 in doi: 10.1016/j.combustflame.2011.10.008)

k_ij = [ 0.000, 0.000 ...
       ; 0.000, 0.000 ]; % If available: set the binary interaction parameter

% initialize a parameter (only the a in a*alpha(T)), b parameter and c
% parameter (which is used in the alpha function of a*alpha(T))
for i=1:nc
    slst(i).a  = a_PR( R_univ, slst(i).Tc, slst(i).pc, slst(i).Zc );  
    slst(i).b  = b_PR( R_univ, slst(i).Tc, slst(i).pc, slst(i).Zc ); 
    slst(i).c  = c_PR( slst(i).omega, slst(i).Zc );  
end

% Solver parameter for fsolve
options=optimset('simplex','off'...
                ,'jacobian','off'...
                ,'LargeScale','on'...
                ,'MaxFunEvals',100 ...
                ,'MaxNodes',2 ...
                ,'TolFun',1.0000e-16 ...
                ,'Tolx', 1.0e-10 ...
                ,'display','off'...
                ,'RelLineSrchBnd',0.05 ...
                ,'RelLineSrchBndDuration',20 ...
                ,'Algorithm','trust-region-reflective');      
  
% Calculate adiabatic mixing temperatures
nz = 41;
small = 0.;
z1 = linspace(0+small,1-small,nz); 
z2 = 1 - z1; 


T1 = 900.;     % Temperature of Species 1 (N2) before mixing
T2 = 363.;     % Temperature of Species 2 (C12H26) before mixing
p  = 60e5;     % Pressure


% Calculate EOS parameters for pure components
[aalpha_1, daalphadT_1, ddaalphadT_1, b_1, aalpha_jk_1] = mix_GCEOS(nc, [1.,0.], T1);
[aalpha_2, daalphadT_2, ddaalphadT_2, b_2, aalpha_jk_2] = mix_GCEOS(nc, [0.,1.], T2);

% For pure component 1 & 2: Calculate volume, internal energy, enthalpy,
% cv, cp, speed of sound, and partial molar volume/enthalpy 
[vol_1, e_1 , h_1, cv_1, cp_1, sound_1, dvdx_Tp_1, phase_1] ...
          = prop_GCEOS( u_EOS, w_EOS    ...
                      , aalpha_1, daalphadT_1, ddaalphadT_1, aalpha_jk_1  ...
                      , b_1, slst(1).coeffs, T1, p, 'Gibbs' );
                  
[vol_2, e_2 , h_2, cv_2, cp_2, sound_2, dvdx_Tp_2, phase_2] ...
          = prop_GCEOS( u_EOS, w_EOS    ...
                      , aalpha_2, daalphadT_2, ddaalphadT_2, aalpha_jk_2  ...
                      , b_2, slst(2).coeffs, T2, p, 'Gibbs' );

tic

for i=1:nz
   
    z = [z1(i),z2(i)]; % Overall mole fraction z 

    if i == 1 % Initial guess
        temp = z(1) * T1 + z(2) * T2;
    else
        % Do nothing. temp is the best guess we have.
    end
    
    hTarget = z(1) * h_1 + z(2) * h_2; % Target enthalpy
  
    eps = 1;
    nit = 0;
    
    converged = false;
    
    while nit < 100
        
        nit = nit + 1;
        
        % Calculate EOS parameter for mixture
        [aalpha_m, daalphadT_m, ddaalphadT_m, b_m, aalpha_jk_m] = mix_GCEOS(nc, z, temp);
        
        % Mix ideal reference state
        coeffs_m = zeros(size(slst(1).coeffs));
        for n=1:nc
            coeffs_m = coeffs_m + z(n) * slst(n).coeffs;
        end
        
        % Calculate enthalpy of mixture (here, our mixing rules apply)
        [vol_m, u_m , h_m, cv_m, cp_m, sound_m, dvdx_Tp_m, phase_m] ...
                  = prop_GCEOS( u_EOS, w_EOS    ...
                              , aalpha_m, daalphadT_m, ddaalphadT_m, aalpha_jk_m  ...
                              , b_m, coeffs_m, temp, p, 'Gibbs' );
        
        eps = abs((hTarget-h_m)/hTarget); % Residuum

        if eps <= 1e-6
            converged = true;
            break
        else             
            % Update temperature
            alpha    = 1.E-01;
            delta_h  = (h_m - hTarget);
            dT       = - delta_h / cp_m; 
            C        = 1. / ( 1 + abs(dT * alpha) );
            temp     = temp + dT * C; 
        end             

    end
  
    if not(converged) 
        error('z_1 = %8.4f | T = %6.3f | n = %4i | Resid = %12.6e \n',z(1),temp,nit,eps)
    end 

    M    = z(1) * slst(1).M + z(2) * slst(2).M; % Mixture molar mass 
    Y(1) = z(1) * slst(1).M / M; % Overall mass fraction comp. #1 
    Y(2) = z(2) * slst(2).M / M; % Overall mass fraction comp. #2

    T_F(i)  = temp;  % 'Frozen' adiabatic mixing temperature (T_F)
    
    vol_F(i)    = vol_m;     % Frozen adiabatic mixing volume (m^3/mol)
    vol_F_kg(i) = vol_m / M; % Frozen adiabatic mixing volume (m^3/kg)

    rho_F_kg_1(i) = Y(1) / vol_F_kg(i); % Frozen partial density comp. #1 (N2) 
    rho_F_kg_2(i) = Y(2) / vol_F_kg(i); % Frozen partial density comp. #2 (C12H26) 
  
    % Check stability via the Tangent-Plane-Distance function
    [stable, TPDmin, K_init] = solveTPD_SSI( nc, z, temp, p );
    %[stable, TPDmin, K_init] = solveTPD_BFGS( nc, z, temp, p );
    
    if not(stable) % Calculate equilibrium adiabatic mixing temperature (T_EQ)
        
            formatSpec = '==> Single phase NOT STABLE. \n';
            fprintf(formatSpec);        
 
            T_init = temp; % Initial value from T_F solution
            HPn = @(T) objectiveHPN( T, z, hTarget, p ); % Create function handle for fsolve
            
            % Adjust temperature such that h = psi_v * h_v   + ( 1. - psi_v ) * h_l = hTarget
            [T_EQ(i),fval,exitflag] = fsolve(HPn,T_init,options); 

            if not(any(exitflag==[1 2 3 4]))
                disp('NOT CONVERGED!')
                formatSpec = 'HPn NOT CONVERGED! exitflag = %u | Residuum = %12.6e \n';
                fprintf(formatSpec,exitflag,fval);
                stop
            end
            
            % Get composition of liquid and vapor phases and phase fraction psi_v
            % Phase fraction psi_v on molar basis := mol vapor / mol mixture
            [~, x, y, psi_mol(i), u_l, u_v, h_l, h_v, vol_l, vol_v] ...
                                    = objectiveHPN( T_EQ(i), z, hTarget, p );
            
            vol_EQ(i) = psi_mol(i) * vol_v + ( 1. - psi_mol(i) ) * vol_l;   
   
            M_l = x(1) * slst(1).M + x(2) * slst(2).M; % Mixture molar mass in liquid phase
            M_v = y(1) * slst(1).M + y(2) * slst(2).M; % Mixture molar mass in vapor phase
            % 
            vol_l_kg = vol_l / M_l; % Specific molar volume liquid phase (m^3 kg^-1)
            vol_v_kg = vol_v / M_v; % Specific molar volume vapor phase (m^3 kg^-1)
            % 
            w_l(1) = x(1) * slst(1).M / M_l; % Mass fraction of comp. #1 (N2) in the liquid phase
            w_v(1) = y(1) * slst(1).M / M_v; % Mass fraction of comp. #1 (N2) in the vapor phase
            w_l(2) = x(2) * slst(2).M / M_l; % Mass fraction of comp. #2 (C12H26) in the liquid phase
            w_v(2) = y(2) * slst(2).M / M_v; % Mass fraction of comp. #2 (C12H26) in the vapor phase
            % 
            rho_l_kg = 1. / vol_l_kg; % Density (kg m^-3) of liquid phase
            rho_v_kg = 1. / vol_v_kg; % Density (kg m^-3) of vapor phase 
            % 
            rho_l_kg_1(i)  = rho_l_kg * w_l(1); % Partial density liquid phase of comp. #1 (N2)  
            rho_v_kg_1(i)  = rho_v_kg * w_v(1); % Partial density vapor phase of comp. #1 (N2) 
 
            rho_l_kg_2(i)  = rho_l_kg * w_l(2); % Partial density liquid phase of comp. #2 (C12H26) 
            rho_v_kg_2(i)  = rho_v_kg * w_v(2); % Partial density vapor phase of comp. #2 (C12H26) 
            
            psi_mass   = psi_mol(i) * M_v / M; % Phase fraction psi_v on mass basis := kg vapor / kg mixture

            vol_EQ_kg(i)      = psi_mass * vol_v_kg + ( 1. - psi_mass ) * vol_l_kg;
            
            rho_EQ_kg_1(i)  = 1. / vol_EQ_kg(i) * Y(1); % Overall partial equilibrium density comp. #1 (kg m^-3)
            rho_EQ_mol_1(i) = 1. / vol_EQ(i) * z(1); % Overall partial equilibrium density comp. #1 (mol m^-3)
            
            rho_EQ_kg_2(i)  = 1. / vol_EQ_kg(i) * Y(2);  % Overall partial equilibrium density comp. #2 (kg m^-3)
            rho_EQ_mol_2(i) = 1. / vol_EQ(i) * z(2); % Overall partial equilibrium density comp. #2 (mol m^-3)
            
            
        
    else
 
        fprintf('==> Single phase STABLE. \n');
        
        % Frozen and equilibrium approach collapse outside of two-phase region 
        T_EQ(i)        = temp;  
        vol_EQ(i)      = vol_m;     
        vol_EQ_kg(i)   = vol_m / M; 
        rho_EQ_kg_1(i) = rho_F_kg_1(i);
        rho_EQ_kg_2(i) = rho_F_kg_2(i);  
        
        % There are no liq. and vap. phase densties
        rho_l_kg_1(i)  = -100.; 
        rho_v_kg_1(i)  = -100.;
        rho_l_kg_2(i)  = -100.; 
        rho_v_kg_2(i)  = -100.;
       
    end
             
end;

%% Do some post processing:
font_size = 16;
fcount    = 0; % Counter

% PLOT #1 - All partial densities of comp. #1 (N2): 
fcount  = fcount + 1;
figure(fcount)
fig(1) = plot(z1,rho_F_kg_1,'k-o',...
        'LineWidth',1,...
        'MarkerSize',5,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','w');
hold on   
fig(2) = plot(z1,rho_EQ_kg_1,'k-o',...
        'LineWidth',1,...
        'MarkerSize',5,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','k');
hold on  
fig(3) = plot(z1,rho_l_kg_1,'o',...
        'LineWidth',1,...
        'MarkerSize',5,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','b');
hold on 
fig(4) = plot(z1,rho_v_kg_1,'o',...
        'LineWidth',1,...
        'MarkerSize',5,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','r');
set(gca,'FontSize',font_size)    
xlim([0 1]);
ylim([0 100]); 
legend(fig,{'Frozen','Equilibrium','Liquid','Vapor'},'Location','northwest');
title('Nitrogen partial densities', 'FontSize', font_size)
xlabel('z_{N_2} [mol/mol]')

% PLOT #2 - All partial densities of comp. #2 (C12H26): 
fcount  = fcount + 1;
figure(fcount)
fig(1) = plot(z1,rho_F_kg_2,'k-o',...
        'LineWidth',1,...
        'MarkerSize',5,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','w');
hold on 
fig(2) = plot(z1,rho_EQ_kg_2,'k-o',...
        'LineWidth',1,...
        'MarkerSize',5,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','k');
hold on                 
fig(3) = plot(z1,rho_l_kg_2,'o',...
        'LineWidth',1,...
        'MarkerSize',5,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','b');
hold on 
fig(4) = plot(z1,rho_v_kg_2,'o',...
        'LineWidth',1,...
        'MarkerSize',5,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','r');
set(gca,'FontSize',font_size)    
xlim([0 1]);
ylim([0 700]); 
legend(fig,{'Frozen','Equilibrium','Liquid','Vapor'},'Location','southwest');
title(['n-Dodecane partial densities'], 'FontSize', font_size)
xlabel('z_{N_2} [mol/mol]')


%% Calculate Txy diagram (brute force method) and plot adiabatic mixing temp.
nT   = 256;  % number of T steps for VLE calculation 
T    = linspace(300,700,nT); 

% Create an initial guess for the lowest temperature
[stable, TPDmin, K_init] = solveTPD_SSI( nc, [0.5, 0.5], T(1), p );
[x, y, psi_mol, fval, success, n_it] = solveTPN( nc, [0.5, 0.5], T(1), p, K_init );

xy = [x(1), y(1)];
for m=1:nT
    % Function handle
    VLE = @(xy) objectiveTPBinary(xy, T(m), p);
    % Solve VLE with fsolve
    [xy,fval,exitflag] = fsolve(VLE,xy,options);
    % Error handling
    if not(any(exitflag==[1 2 3 4]))
        error('fsolve did not converge! exitflag = %u | residuum = %12.6e \n',exitflag,fval)
    end   
    
    if abs(xy(1)-xy(2)) <= 1.E-14
       break % Trivial solution (x=y)
    end 
    
    x(m)   = xy(1); % Mole fraction of comp. #1 in liq. phase
    y(m)   = xy(2); % mole fraction of comp. #1 in vap. phase
    Txy(m) = T(m); % Remember the temperature that belongs to x & y
    formatSpec = 'T = %6.3f | p = %8.4e | x = %6.3f (liq.) | y = %6.3f (vap.)\n';
    fprintf(formatSpec,T(m),p,xy(1),xy(2));   
end 

figure(fcount+1)
% Plot bubble-point line:
fig(1) = plot(x,Txy,'b-',...
                'LineWidth',1);
hold on
% Plot dew-point line:
fig(2) = plot(y,Txy,'r-',...
                'LineWidth',1);
hold on
% Plot frozen adiab. mixture line:
fig(3) = plot(z1,T_F,'k-o',...
        'LineWidth',1,...
        'MarkerSize',5,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','w');
hold on
% Plot equilibrium adiab. mixing line:
fig(4) = plot(z1,T_EQ,'k-o',...
        'LineWidth',1,...
        'MarkerSize',5,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','k');
xlim([0 1]);
ylim([300 1000])   
set(gca,'FontSize',font_size)
legend('Liq','Vap','T_F','T_{EQ}','Location','northeast')
title('Frozen/Equilibrium adiabatic mixing temperature', 'FontSize', font_size)
xlabel('x_{N_2},y_{N_2},z_{N_2} [mol/mol]')

