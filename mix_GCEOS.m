function [aalpha, daalpha_T, ddaalpha_T, b, daalpha_k, aalpha_ik] = mix_GCEOS( nc, xyz, temp )
% MIX_GCEOS   Calculate mixture parameters for cubic EOS.
%
% Inputs: 
%    nc               - Number of components
%    xyz              - Mole fraction (can be liquid x, vapor y or overall z)
%    temp             - Temperature
%
% Outputs:
%    aalpha           - a*alpha(T) value in GCEOS
%    daalpha_T        - 1st derivative of  a*alpha(T) with respect to temperature 
%    ddaalpha_T       - 2nd derivative of  a*alpha(T) with respect to temperature 
%    b                - Co-volume "b" in GCEOS
%    daalpha_k(nc)    - Vector with summe_{j=1}^{N_c}(z_j*a_jk*alpha_jk) for component k
%    aalpha_ik(nc,nc) - Matrix with a_ij * alpha_ij entries

% Author: 
%    Jan Matheis, Technical University Munich, jan.matheis@gmail.com
%
% Literature:
%    REF1: J. Richard Elliott, Carl T. Lira. Introductory Chemical Engineering Thermodynamics 




global crule
global slst
global k_ij
global R_univ

% REF1: "Introductory Chemical Engineering Thermodynamics", J. Richard Elliott, Carl T. Lira

aalpha     = 0.; % a*alpha(T) value in GCEOS
daalpha_T  = 0.; % 1st derivative with respect to temperature 
ddaalpha_T = 0.; % 2nd derivative with respect to temperature

daalpha_k  = zeros(1,nc);  % Sum in Eqn. 15.34 in REF1 (vector with nc components)
aalpha_ik  = zeros(nc,nc); % Matrix with every single a_ij * alpha_ij

b          = 0.;           % Co-volume "b" in GCEOS

% Pseudo-critical method): Adjustment of pseudo-critical properties. 
% Off-diagonal elements are calculated using the same expression as for the diagonals.
if strcmp(crule,'CRITICAL')
    
    for i=1:nc
        
        work = 0.;
        
        for j=1:nc
            
            if i == j
                
                a_ij = slst(i).a;
                [alpha_ij, dalphadT_ij, ddalphadT_ij] = alpha( temp, slst(i).Tc, slst(i).c );
            
            else
                
               vc_ij    = 0.125 * ( slst(i).vc^(1/3) + slst(j).vc^(1/3))^3;
               Zc_ij    = 0.5   * ( slst(i).Zc       + slst(j).Zc );
           
               Tc_ij    = sqrt( slst(i).Tc * slst(j).Tc ) * ( 1 -  k_ij(i,j));  

               pc_ij    = Zc_ij * R_univ * Tc_ij / vc_ij;
               omega_ij = 0.5 * ( slst(i).omega + slst(j).omega );
               c_ij     = c_PR( omega_ij, Zc_ij );
               a_ij     = a_PR( R_univ, Tc_ij, pc_ij, Zc_ij );
               [alpha_ij, dalphadT_ij, ddalphadT_ij] = alpha( temp, Tc_ij, c_ij );
            
            end
                           
            aalpha     = aalpha     + xyz(i) * xyz(j) * a_ij * alpha_ij;	
            daalpha_T  = daalpha_T  + xyz(i) * xyz(j) * a_ij * dalphadT_ij;
            ddaalpha_T = ddaalpha_T + xyz(i) * xyz(j) * a_ij * ddalphadT_ij;
            work       = work       +          xyz(j) * a_ij * alpha_ij;
            
            aalpha_ik(i,j) = a_ij * alpha_ij;

        end

        b             = b + xyz(i) * slst(i).b;
        daalpha_k(i)  = work;

    end
    
else
    
    error('Unkown combination rule %s.',crule)
    
end


