function tpd = evalTPD( nc, w_trial, z_test, temp, press )
% EVALTPD   Evaluate tpd function for composition z
%
% Inputs: 
%    nc           - Number of components
%    w_trial(nc)  - Trial phase composition 
%    z_test(nc)   - Composition z that is tested  
%    temp, press  - Temperature and pressure
%
% Outputs:
%    tpd          - non-dimensional tpd-function
%
% Author: 
%    Jan Matheis, Technical University Munich, jan.matheis@gmail.com
%
% Literature:
% REF1: Michelsen (1982) The isothermal flash problem. Part I. Stability
% REF2: Hoteit & Firoozabadi (2006) Simple phase stability-testing algorithm in the reduction method
% REF3: Li & Firoozabadi (2012) General Strategy for Stability Testing and Phase-Split Calculation in Two and Three Phases

global R_univ
global u_EOS w_EOS
global slst

% Get mixed EOS params of assumed single phase (z_test)
[aalpha_m, ~, ~, b_m, aalpha_jk_m, ~] = mix_GCEOS( nc, z_test, temp );

% Get compressibility factor of assumed single phase (z_test) 
[vol_m, ~] = vol_GCEOS( u_EOS, w_EOS, aalpha_m, b_m, temp, press, 'Gibbs' );
Z_m = vol_m(1) * press / ( R_univ * temp );

% Calculate fugacity coeffcients of assumed single phase (z_test) 
for i=1:nc; 
   lnphi_m(i) = lnphi_GCEOS( u_EOS, w_EOS, slst(i).b, b_m, aalpha_jk_m(i), aalpha_m, Z_m, temp, press );
   d(i)       = log( z_test(i) ) + lnphi_m(i); % Eqn. 3 in REF2
end

% Calculate non-dimensional tpd function value
tpd = 0.;
[aalpha_w, ~, ~, b_w, aalpha_jk_w] = mix_GCEOS( nc, w_trial, temp );
[vol_w, ~] = vol_GCEOS( u_EOS, w_EOS, aalpha_w, b_w, temp, press, 'Gibbs' );
Z_w = vol_w(1) * press / ( R_univ * temp );

for i = 1:nc
    lnphi_w = lnphi_GCEOS( u_EOS, w_EOS, slst(i).b, b_w, aalpha_jk_w(i), aalpha_w, Z_w, temp, press );
    tpd     = tpd + w_trial(i) * ( lnphi_w + log( w_trial(i) ) - d(i) );
end

